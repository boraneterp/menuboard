import React from 'react';
import { Layout, Row, Col } from 'antd';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Route, Switch, Link} from 'react-router-dom';

function Layout1(){
  return(
    <div>
      <h2>Layout1</h2>
      Layout1...
      <Layout>
        <Row>
        <Col span={16} >
          <div className="mainTop">              
             </div>         
             <div className="mainBottom">            
             </div>
          </Col> 
          <Col span={8}>
            <div id="rightTop"> 
                <div className="r_top">
                </div>
              </div>
              <div id="rightMid"> 
                <div className="r_mid">
                </div>
              </div>
                <div id="rightBot"> 
                  <div className="r_bottom">
                  </div>
              </div>          
          </Col>
        </Row>   
    </Layout>
    </div>
  )
}

function Layout2(){
  return(
    <div>
      <h2>Layout2</h2>
      Layout2...
      <Layout>     
        <Row>
        <Col span={8} >
          <div id="mainTop">
            <div className="mainTop">                                
            </div>
          </div>
          <div id="mainMid">
            <div className="mainMid">
            </div>
          </div>
          <div id="mainBottom">
            <div className="mainBottom">             
              <div className="imageContainer"> 
              </div>              
            </div>  
          </div>
          </Col> 
          <Col span={16}>
            <div id="rightTop"> 
                <div className="r_top">
                </div>
              </div>
              <div id="rightMid"> 
                <div className="r_mid">                
                </div>
              </div>
                <div id="rightBot"> 
                  <div className="r_bottom">
                  </div>
              </div>          
          </Col>
        </Row>    
    </Layout>
    </div>
  )
}

function Layout3(){
  return(
    <div>
      <h2>Layout3</h2>
      Layout3...
       <Layout>     
        <Row>
        <Col span={8} >
          <div className="mainTop">
             </div>             
             <div className="mainBottom">            
                 <div className="imageContainer"> 
                 </div>            
             </div>
          </Col> 
          <Col span={8}>
            <div id="rightTop"> 
                <div className="r_top">
                </div>
              </div>
              <div id="rightMid"> 
                <div className="r_mid">
                </div>
              </div>
                <div id="rightBot"> 
                  <div className="r_bottom">
                  </div>
              </div>         
          </Col>
        <Col span={8}>
          <div id="Top3">            
            <div className="Top_3">
            </div>
          </div>
          <div id="Bot3">
            <div className="Bot_3">
            </div>            
          </div>
        </Col>
        </Row>
      </Layout>
    </div>
  )
}

function App(){
  return (
    <div>
      <h1>Manual Layout</h1>
      <ul>
        <li><Link to="/layout1">Layout1</Link></li>
        <li><Link to="/layout2">Layout2</Link></li>
        <li><Link to="/layout3">Layout3</Link></li>
      </ul>
      <Switch>
        <Route exact path="/layout1"><Layout1></Layout1></Route>
        <Route path="/layout2"><Layout2></Layout2></Route>
        <Route path="/layout3"><Layout3></Layout3></Route>
        <Route path="/">Not Found</Route>
      </Switch>
    </div>
  )
}

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
